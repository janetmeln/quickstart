<?php

class Application_Model_UsersMapper
{

    protected $_dbTable;
 
    public function setDbTable($dbTable)
    {
        if (is_string($dbTable)) {
            $dbTable = new $dbTable();
        }
        if (!$dbTable instanceof Zend_Db_Table_Abstract) {
            throw new Exception('Invalid table data gateway provided');
        }
        $this->_dbTable = $dbTable;

        return $this;

    }
 
    public function getDbTable()
    {
        if (null === $this->_dbTable) {
            $this->setDbTable('Application_Model_DbTable_Users');
        }

        return $this->_dbTable;

    }
 
    public function save(Application_Model_Users $users)
    {
        $data = array(
            'email' => $users->getEmail(),
            'username' => $users->getUsername(),
            'firstname' => $users->getFirstname(),
            'lastname' => $users->getLastname(),
            'password' => $users->getPassword()
        );
 
        if (null === ($id = $users->getId())) {
            unset($data['id']);
            $this->getDbTable()->insert($data);
        } else {
            $this->getDbTable()->update($data, array('id = ?' => $id));
        }

    }
 
    public function find($id, Application_Model_Users $users)
    {
        $result = $this->getDbTable()->find($id);
        if (0 == count($result)) {

            return;
            
        }
        $row = $result->current();
        $users->setId($row->id)
                  ->setEmail($row->email)
                  ->setUsername($row->username)
                  ->setUsername($row->firstname)
                  ->setUsername($row->lastname)
                  ->setFirstname($row->password);
    }
 
    public function fetchAll()
    {
        $resultSet = $this->getDbTable()->fetchAll();
        $entries   = array();

        foreach ($resultSet as $row) {
            $entry = new Application_Model_Users();
            $entry->setId($row->id)
                  ->setEmail($row->email)
                  ->setUsername($row->username)
                  ->setFirstname($row->firstname)
                  ->setLastname($row->lastname)
                  ->setPassword($row->password);
            $entries[] = $entry;
        }

        return $entries;
    }

}

