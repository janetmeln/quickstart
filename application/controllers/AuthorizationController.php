<?php

class AuthorizationController extends Zend_Controller_Action
{


    public function indexAction()
    {
        $this->_helper->redirector('login');
    }

    public function loginAction()
    {
        if (Zend_Auth::getInstance()->hasIdentity()){
            $this->_redirect('/');
        }

        $request = $this->getRequest();
        $form = new Application_Form_Login();
        $this->view->form = $form;    

        if ($request->isPost()) { 

            if ($form->isValid($request->getPost())) {
                $authAdapter = new Zend_Auth_Adapter_DbTable(Zend_DB_Table::getDefaultAdapter());
                $authAdapter->setTableName('users')
                    ->setIdentityColumn('username')
                    ->setCredentialColumn('password');

                $username = $request->getPost('username');
                $password = $request->getPost('password');

                $authAdapter->setIdentity($username)
                    ->setCredential(md5($password));

                $auth = Zend_Auth::getInstance();
                $result = $auth->authenticate($authAdapter);

               if($result->isValid()){
                   $this->_redirect('/'); 
                }else{
                    echo $this->view->headScript()->appendFile($this->view->baseUrl().'/js/script.js');
                }
            }
        }    
    }

    public function logoutAction()
    {
        Zend_Auth::getInstance()->ClearIdentity();
        $this->_redirect('/');
    }


}





