<?php

class UsersController extends Zend_Controller_Action
{

    public function indexAction()
    {

        $request = $this->getRequest();
        $form = new Application_Form_Signup();

        if ($this->getRequest()->isPost()) {
            if ($form->isValid($request->getPost())) {
                $user = new Application_Model_Users($form->getValues());
                $mapper  = new Application_Model_UsersMapper();
                $mapper->save($user);
                $this->_helper->getHelper('FlashMessenger')
                ->addMessage('Thank you for your submission');
                $this->_redirect('/users/success');
            }
        }
        $this->view->form = $form;
    }

    public function successAction()
    {

        if ($this->_helper->getHelper('FlashMessenger')->getMessages()) {
          $this->view->messages = $this->_helper->getHelper('FlashMessenger')->getMessages();    
        } else {
          $this->_redirect('/');    
        }
        
    }

    public function userslistAction()
    {
        $users = new Application_Model_UsersMapper();
        $this->view->entries = $users->fetchAll();
    }    
}

