<?php

class Application_Form_Login extends Zend_Form
{

    public function init()
    {
        
    	// Set the method for the display form to POST
    	$this->setMethod('post');
        $this->setName('login');

        // Add an login element
        $this->addElement('text', 'username', array(
            'label'      => 'Login:',
            'required'   => true,
            'filters'    => array('StringTrim', 'HtmlEntities'),
            'validators' => array(
                'NotEmpty',
            )
        )); 

         // Add an password element
        $this->addElement('password', 'password', array(
            'label'      => 'Password:',
            'required'   => true,
            'filters'    => array('StringTrim'),
            'validators' => array(
                'NotEmpty',
            )            
        ));

        // Add the submit button
        $this->addElement('submit', 'submit', array(
            'ignore'   => true,
            'label'    => 'Sing Up',
        ));   

    }


}

