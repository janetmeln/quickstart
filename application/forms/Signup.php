<?php

class Application_Form_Signup extends Zend_Form
{

    public function init()
    {
	    // Set the method for the display form to POST
	    $this->setMethod('post');

        // Add an login element
        $this->addElement('text', 'username', array(
            'label'      => 'Login:',
            'required'   => true,
            'filters'    => array('StringTrim', 'HtmlEntities'),
            'validators' => array(
                'NotEmpty',
            )
        )); 

        // Add an firstname element
        $this->addElement('text', 'firstname', array(
            'label'      => 'FirstName:',
            'required'   => true,
            'filters'    => array('StringTrim', 'HtmlEntities'),
            'validators' => array(
                'Alpha', 'NotEmpty',
            )    
        ));

         // Add an lastname element
        $this->addElement('text', 'lastname', array(
            'label'      => 'LastName:',
            'required'   => true,
            'filters'    => array('StringTrim', 'HtmlEntities'),
            'validators' => array(
                'Alpha',  'NotEmpty',
            )
        ));
 
         // Add an password element
        $this->addElement('password', 'password', array(
            'label'      => 'Password:',
            'required'   => true,
            'filters'    => array('StringTrim'),
            'validators' => array(
                'NotEmpty',
            )            
        ));
 
 
        // Add an email element
        $this->addElement('text', 'email', array(
            'label'      => 'Your email address:',
            'required'   => true,
            'filters'    => array('StringTrim', 'HtmlEntities'),
            'validators' => array(
                'EmailAddress', 'NotEmpty',
            )
        ));
 
        // Add a captcha
        $this->addElement('captcha', 'captcha', array(
            'label'      => 'Please enter the 5 letters displayed below:',
            'required'   => true,
            'captcha'    => array(
                'captcha' => 'Figlet',
                'wordLen' => 5,
                'timeout' => 300
            )
        ));

        // Add the submit button
        $this->addElement('submit', 'submit', array(
            'ignore'   => true,
            'label'    => 'Sing Up',
        ));   

         // And finally add some CSRF protection
        $this->addElement('hash', 'csrf', array(
            'ignore' => true,
        ));
    }


}

